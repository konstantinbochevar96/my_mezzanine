# This file is exec'd from settings.py, so it has access to and can
# modify all the variables in settings.py.
import os


# Make these unique, and don't share it with anybody.
SECRET_KEY = "i1nd#sa(2^ovmyu*qer@*pl(hn134ruj=u6pvgsq6!c+_&%+v8"
NEVERCACHE_KEY = "ow0a#@w=kipp=7!^3ss(#an8wf4-9v8-@d0teem9$5v+mw^9(1"

# DATABASES = {
#     "default": {
#         # Ends with "postgresql_psycopg2", "mysql", "sqlite3" or "oracle".
#         "ENGINE": "django.db.backends.sqlite3",
#         # DB name or path to database file if using sqlite3.
#         "NAME": "dev.db",
#         # Not used with sqlite3.
#         "USER": "",
#         # Not used with sqlite3.
#         "PASSWORD": "",
#         # Set to empty string for localhost. Not used with sqlite3.
#         "HOST": "",
#         # Set to empty string for default. Not used with sqlite3.
#         "PORT": "",
#     }
# }

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ['DB_NAME'],
        'HOST': os.environ['DB_HOST'],
        'USER': os.environ['DB_USER'],
        'PORT': os.environ['DB_PORT'],
        'PASSWORD': os.environ['DB_PASSWORD'],
    }
}

# Allowed development hosts
ALLOWED_HOSTS = ["localhost", "127.0.0.1", "::1"]

MEDIA_ROOT = '/var/www/mezzanine/media'
STATIC_ROOT = '/var/www/mezzanine/static'
###################
# DEPLOY SETTINGS #
###################

# These settings are used by the default fabfile.py provided.
# Check fabfile.py for defaults.

# FABRIC = {
#     "DEPLOY_TOOL": "rsync",  # Deploy with "git", "hg", or "rsync"
#     "SSH_USER": "",  # VPS SSH username
#     "HOSTS": [""],  # The IP address of your VPS
#     "DOMAINS": [""],  # Will be used as ALLOWED_HOSTS in production
#     "REQUIREMENTS_PATH": "requirements.txt",  # Project's pip requirements
#     "LOCALE": "en_US.UTF-8",  # Should end with ".UTF-8"
#     "DB_PASS": "",  # Live database password
#     "ADMIN_PASS": "",  # Live admin user password
#     "SECRET_KEY": SECRET_KEY,
#     "NEVERCACHE_KEY": NEVERCACHE_KEY,
# }
